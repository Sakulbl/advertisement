<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
Use Symfony\Component\Routing\Annotation\Route;

/**
 * HomeController
 *
 * @author lukas
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $advertisements = $em->getRepository('App:Advertisement')->findAll();

        return $this->render('default/index.html.twig', [
            'advertisements' => $advertisements,
        ]);
    }
}

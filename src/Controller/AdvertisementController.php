<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
Use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Advertisement;
use App\Form\AdvertisementType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * AdvertisementController
 *
 * @Route("/advertisement")
 *
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 *
 * @author lukas
 */
class AdvertisementController extends Controller
{
    /**
     * View user advertisements
     *
     * @Route("/view", name="view_user_advertisements")
     */
    public function viewUserAdvertisementsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $advertisements = $em->getRepository('App:Advertisement')->findByUser($this->getUser());

        return $this->render('default/index.html.twig', [
            'advertisements' => $advertisements,
        ]);
    }

    /**
     * Create advertisement
     *
     * @Route("/create", name="create_advertisement")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $advertisement = new Advertisement();
        $advertisement->setUser($this->getUser());
        $form = $this->createForm(AdvertisementType::class, $advertisement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $advertisement = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($advertisement);
            $em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('advertisement/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
